package com.zhuawa.course.biz.user.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhuawa.course.biz.user.customerplugin.WebLog;
import com.zhuawa.course.biz.user.service.UserService;
import com.zhuawa.course.persistence.user.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author apple on 2019/12/14.
 * @version 1.0
 */
@RestController
@PropertySource(value = "classpath:customer.properties")
public class UserController {
    @Resource
    private UserService userService;
    @Value("${com.zhuwa.course.customerName}")
    private String aa;

    @RequestMapping("/user")
    @WebLog(description = "user 测试")
    public IPage<User> userTest(@RequestParam String word) {
        //当前页
        int currentPage = 1;
        //每页大小
        int pageSize = 20;
        Page page = new Page(currentPage, pageSize);
        IPage<User> userIPage = userService.UserIpageDS(page);
        System.out.println(userIPage.getSize());
        System.out.println(word + aa);
        return userIPage;
    }
}
